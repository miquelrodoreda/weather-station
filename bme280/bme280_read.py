# pip install RPi.bme280

import smbus2
import bme280
import time
from datetime import datetime

def main():
    port = 1
    address = 0x76
    bus = smbus2.SMBus(port)

    calibration_params = bme280.load_calibration_params(bus, address)

    while True:
        data = bme280.sample(bus, address, calibration_params)
        temperature = data.temperature
        humidity = data.humidity
        pressure = data.pressure
        date_time = data.timestamp
        
        now = datetime.now() # current date and time 
        date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
        print("date and time:",date_time)
        print("temperature = %.2f ºC" %temperature)
        print("humidity = %.2f %%" %humidity)
        print("pressure = %.2f hPa" %pressure)
        print("")
        time.sleep(5)

if __name__=='__main__':
    main()
