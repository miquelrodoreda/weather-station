import RPi.GPIO as GPIO    #importem la llibreria RPi.GPIO com a GPIO

import math                #importem la llibreria math, per realitzar operacions que no s'inclouen en les funcions bàsiques de Python

PIN = 5     #definim el pin que llegeix la raspberry a l'executar el programa com a 6

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)   #configurem el pin 5 i el mode de GPIO

rotations = 0.0       #definim la variable rotations, per comptar les voltes de les cassoletes
interval = 5          #definim l'interval de mesura del vent a 5 segons
radius_m = 0.09       #el radi de l'anemòmetre és de 9 cm (0.09 metres)
adjustment = 1.18     #factor de correcció de la mesura, que compensa la pèrdua per fregament

def cb(channel):                #definim la funció cb, que suma 0.5 a la variable rotations
    global rotations
    rotations = rotations + 0.5

def speed_calculation(time_sec):          #definim la funció speed_calculation, que a partir de la variable rotations i interval, calcula la velocitat i la retorna en m/s
    global rotations
    circumference_m = 2 * math.pi * radius_m
    dist_m = circumference_m * rotations
    speed_ms = dist_m / interval
    speed = round(speed_ms * adjustment, 2)
    return speed

GPIO.add_event_detect(PIN, GPIO.FALLING, callback=cb, bouncetime=10) #funció que detecta quan entra corrent al pin 5

def read_speed():                         #definim la funció read_speed, que retorna la velocitat i posa la variable rotations a 0 cada 5 segons (interval)
    global rotations
    result = speed_calculation(interval)
    rotations = 0
    return result
