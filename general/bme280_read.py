# pip install RPi.bme280       #informem que pel funcionament del programa hem d'haver instal·lat prèviament la llibreria RPi.bme280

import smbus2                  #importem les llibreries smbus2,
import bme280                                           #bme280,
import time                                             #time,
from datetime import datetime                           #datetime

def read_data():               #definim la funció read_data
    
    port = 1                   #definim la variable port amb el valor 1
    address = 0x76             #definim l'adreça 
    bus = smbus2.SMBus(port)   #definim la variable bus

    calibration_params = bme280.load_calibration_params(bus, address)  #definim la variable calibration_params

    data = bme280.sample(bus, address, calibration_params)             #definim la variable data com la mesura del sensor
    
    return data                #fem que aquesta funció ens retorni la variable data
