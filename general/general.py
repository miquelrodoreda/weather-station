import time                #importem la llibreria time
import mysql.connector     #importem la llibreria mysql.connector que hem hagut de descarregar

import bme280_read         #importem els programes que hem creat (bme280, pluviòmetre i anemòmetre)
import gauge
import anemometer

def main():                    #definim la funció principal main
    mydb = mysql.connector.connect(   #introduïm els paràmetres de la base de dades que utilitzarem
        host="localhost",
        user="pi",
        password="raspberry",
        database="weather"
    )
    mycursor = mydb.cursor()
    
    while True:    #creem un bucle per tal que el codi de dins es repeteixi infinitament
        data = bme280_read.read_data()       #definim la variable data com el resultat de la funció read_data del programa bme280_read
        rainfall = gauge.read_rain()         #definim la variable rainfall com el resultat de la funció read_rain del programa gauge
        wind_speed = anemometer.read_speed() #definim la variable wind_speed com el resultat de la funció read_speed del programa anemometer
        sql = "INSERT INTO measurement (temperature, pressure, humidity, wind_speed, wind_direction, rainfall) VALUES (%s, %s, %s, %s, %s, %s)"  #establim el format del resultat de cada mesura en les columnes de la taula measurements
        val = (data.temperature, data.pressure, data.humidity, wind_speed, 0.0, rainfall)   #establim quins valors prenen les variables. El valor de wind_direction és 0 perquè finalment no s'utilitza el penell
        mycursor.execute(sql, val)
        mydb.commit()
        print("ok")      #imprimim "ok" per comprovar si s'ha executat amb èxit el programa
        time.sleep(120)  #esperem 120 segons (2 minuts) abans de tornar a començar el programa de l'interior del bucle
        
    
main()
