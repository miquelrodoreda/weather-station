#!/usr/bin/python3
import RPi.GPIO as GPIO
import time
from datetime import datetime
# this many mm per bucket tip
capacity = 0.2794
# which GPIO pin the gauge is connected to
PIN = 6

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# variable to keep track of how much rain
rain = 0

# the call back function for each bucket tip
def cb(channel):
    global rain
    rain = rain + capacity

# register the call back for pin interrupts
GPIO.add_event_detect(PIN, GPIO.FALLING, callback=cb, bouncetime=300)

while True:
    now = datetime.now() # current date and time
    date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
    print("date and time: ", date_time)
    print("rain = %.3f" %rain)
    print("")
    time.sleep(5)

if __name__=='__main__':
    main()