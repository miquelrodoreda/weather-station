r1= 120000
vin = 3.3

resistances = [64400, 39200, 99700, 54400, 119400, 33800, 47100, 27200]

def voltage_divider(r1, r2, vin):
    vout = (vin * r1)/(r1 + r2)
    return round(vout, 2)

for x in range(len(resistances)):
    print(resistances[x], voltage_divider(r1, resistances[x], 3.3))