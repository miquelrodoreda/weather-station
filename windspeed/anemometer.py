import RPi.GPIO as GPIO
import time
from datetime import datetime
import math

PIN = 6

GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# variable to keep track of how much rotations
rotations = 0
interval = 5
radius_m = 0.09
adjustment = 1.18
# the call back function for each bucket tip
def cb(channel):
    global rotations
    rotations = rotations + 0.5

def speed_calculation(time_sec):
    global rotations
    circumference_m = 2 * math.pi * radius_m
    dist_m = circumference_m * rotations
    speed_ms = dist_m / interval
    speed = round(speed_ms * adjustment, 2)
    return speed

# register the call back for pin interrupts
GPIO.add_event_detect(PIN, GPIO.FALLING, callback=cb, bouncetime=10)

while True:
    now = datetime.now() # current date and time
    date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
    print("date and time: ", date_time)
    print("speed = " ,speed_calculation(interval), "m/s")
    print("")
    rotations = 0
    time.sleep(5)

if __name__=='__main__':
    main()